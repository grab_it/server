import WebSocket from 'ws';
import _ from 'lodash';

class WebSocketsServer {
  constructor() {
    this.onConnected = this.onConnected.bind(this);
    this.onMessage = this.onMessage.bind(this);
    this.onError = this.onError.bind(this);
    this.wss = null;
  }
  start({ port }) {
    this.wss = new WebSocket.Server({ port });
    this.wss.on('connection', this.onConnected);
    this.wss.on('error', this.onError);
    console.log(`Listening to AR device data on port ${port} (Websockets)`);
  }
  stop() {
    this.wss.close();
  }
  onConnected(ws) {
    ws.on('message', _.partial(this.onMessage, ws));
    ws.on('error', this.onError);
    this.sendMessage(ws, 'connected');
  }
  onError(error) {
    console.dir(error);
  }
  onMessage(ws, rawMessage) {
    // const message = JSON.parse(rawMessage);
    // events.emit('webSocketsMessageReceived', { message });
  }
  sendMessage(ws, type, payload) {
    const message = JSON.stringify({ ...payload, type });
    ws.send(message);
  }
  broadcast(sender, type, payload) {
    // console.log(`broadcasting ${type}:${payload}`);
    // Broadcast message to everyone excluding the sender.
    this.wss.clients.forEach(client => {
      if (client !== sender && client.readyState === WebSocket.OPEN) {
        this.sendMessage(client, type, payload);
      }
    });
  }
}

export default WebSocketsServer;
