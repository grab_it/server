import Myo from 'myo';
import _ from 'lodash';
import ws from 'ws';

// const sampleRate = 50;
const defaultArm = 'left';

Myo.onError = error => error.code !== 'ECONNREFUSED' && console.dir(error);

class MyoBandReceiver {
  constructor({ onPose }) {
    this.connected = false;
    this.onPose = onPose;
  }
  start() {
    this.connect();
  }
  stop() {
    this.disconnect();
  }
  connect() {
    Myo.on('connected', this.onConnected.bind(this));
    Myo.on('disconnected', this.onDisconnected.bind(this));
    Myo.connect('com.grabit.demo', ws);
    console.log('Listening to Myo Band device input');
  }
  disconnect() {
    Myo.disconnect();
    Myo.off('connected');
    Myo.off('disconnected');
    console.log('Stopped listening to Myo Band device input');
  }
  onConnected(myo, timestamp) {
    console.log('Myo connected');
    this.connected = true;
    const newMyo = _.find(Myo.myos, { macAddress: myo.mac_address });
    if (!newMyo) {
      return;
    }
    // newMyo.on('imu', _.throttle(_.partial(this.onImu, this), sampleRate));
    // newMyo.on('double_tap', this.resetOrientation.bind(this));
    // newMyo.on('wave_in', this.onWaveLeft.bind(this));
    // newMyo.on('wave_out', this.onWaveRight.bind(this));
    newMyo.on('pose', this.onPose);
  }
  onDisconnected(myo, timestamp) {
    const disconnectedMyo = _.find(Myo.myos, { macAddress: myo.mac_address });
    if (!disconnectedMyo) {
      return;
    }
    disconnectedMyo.off('vector');
    disconnectedMyo.off('double_tap');
    this.connected = false;
  }
  onImu(receiver, { orientation, accelerometer, gyroscope }, timestamp) {
    const myo = this;
    const message = {
      deviceType: 'MyoBand',
      deviceId: 'MyoBand',
      type: 'update',
      arm: myo.arm || defaultArm,
      orientation,
      accelerometer,
      gyroscope,
      orientationOffset: myo.orientationOffset
    };
    receiver.onMessage(message);
  }
  onMessage(message) {
    // this.connected && console.log(message);
  }
  resetOrientation() {
    console.log('doubleTap');
    Myo.myos.forEach(myo => myo.zeroOrientation());
  }
  // onWaveLeft() {
  //   console.log('waveLeft');
  // }
  // onWaveRight() {
  //   console.log('waveRight');
  // }
  // onPose(pose) {
  //   console.log(pose);
  // }
}

export default MyoBandReceiver;
