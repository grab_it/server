import MyoReceiver from './api/MyoReceiver';
import WebSocketsServer from './api/WebSocketsServer';
// import app from './app';

// const { PORT = 8080 } = process.env;
// app.listen(PORT, () => console.log(`Listening on port ${PORT}`)); // eslint-disable-line no-console

const wss = new WebSocketsServer();
wss.start({ port: 4120 });

const onPose = pose => wss.broadcast(null, 'pose', { pose });

const myo = new MyoReceiver({ onPose });
myo.start();
