module.exports = {
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module',
    ecmaVersion: 2017
  },
  extends: ['standard'],
  env: {
    node: true,
    browser: true
  },
  rules: {
    'comma-dangle': 0,
    'no-return-assign': 0,
    'import/no-unresolved': 'error',
    semi: [2, 'always'],
    'space-before-function-paren': 0
  },
  plugins: ['import']
};
